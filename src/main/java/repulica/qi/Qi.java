package repulica.qi;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repulica.qi.init.QiItems;
import repulica.qi.init.QiMechanics;

import net.fabricmc.api.ModInitializer;

public class Qi implements ModInitializer {
	public static final String MODID = "qi";

	public static final Logger LOGGER = LogManager.getLogger(MODID);

	@Override
	public void onInitialize() {
		QiItems.init();
		QiMechanics.init();
	}
}
