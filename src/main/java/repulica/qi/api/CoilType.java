package repulica.qi.api;

import org.jetbrains.annotations.Nullable;

import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

//TODO: other coil types? unenum it?
public enum CoilType {
	LEGAL(null, 60),
	ILLEGAL(new TranslatableText("qi.coil.illegal").formatted(Formatting.RED), 20),
	KEY(new TranslatableText("qi.coil.key").formatted(Formatting.AQUA), 240),
	MIRAI(new TranslatableText("qi.coil.mirai").formatted(Formatting.GREEN), -1);

	@Nullable
	private final Text name;

	private final int decayRate;

	CoilType(@Nullable Text name, int decayRate) {
		this.name = name;
		this.decayRate = decayRate;
	}

	@Nullable
	public Text getName() {
		return name;
	}

	public int getDecayRate() {
		return decayRate;
	}
}
