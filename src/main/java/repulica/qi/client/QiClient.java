package repulica.qi.client;

import repulica.qi.api.CoilComponent;
import repulica.qi.init.QiItems;
import repulica.qi.init.QiMechanics;

import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;

@Environment(EnvType.CLIENT)
public class QiClient implements ClientModInitializer {
	@Override
	public void onInitializeClient() {
		ColorProviderRegistry.ITEM.register((stack, index) -> {
			if (index == 0) return 0xFFFFFF;
			PlayerEntity player = MinecraftClient.getInstance().player;
			if (player != null && !stack.getOrCreateTag().getBoolean("AlwaysGlow")) {
				if (player.hasStatusEffect(QiMechanics.CHARGED)) {
					for (int i = 0; i < player.inventory.size(); i++) {
						if (player.inventory.getStack(i) == stack) {
							return getCoilColor(stack);
						}
					}
				}
			} else {
				return getCoilColor(stack);
			}
			return 0;
		}, QiItems.COIL);
	}

	public static int getCoilColor(ItemStack stack) {
		CoilComponent component = QiMechanics.COIL.get(stack);
		float hue = component.getStability() / 200f;
		float powerMul = component.getPower() / 100f;
		float decis = System.nanoTime() / 100_000_000f;
		float value = (float) (Math.sin(decis * powerMul) + 2) / 3;
		return MathHelper.hsvToRgb(hue, 1f, value);
	}
}
