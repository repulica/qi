package repulica.qi.impl;

import dev.onyxstudios.cca.api.v3.component.AutoSyncedComponent;
import repulica.qi.api.CoilComponent;
import repulica.qi.api.CoilType;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;

public class MachineCoilComponent implements CoilComponent, AutoSyncedComponent {
	private int power = 75;
	private int stability = 100;
	private CoilType type = CoilType.LEGAL;
	private int stabilityTicks = 0;

	public MachineCoilComponent(ItemStack stack) { }

	public void initialize(CoilComponent coil) {
		this.power = coil.getPower();
		this.stability = coil.getStability();
		this.type = coil.getType();
	}

	public boolean use() {
		if (type == CoilType.MIRAI) return false; //無限のエネルギ
		stabilityTicks++;
		if (stabilityTicks >= type.getDecayRate()) {
			stability--;
			stabilityTicks = 0;
		}
		return stability <= 0;
	}

	@Override
	public boolean isMachine() {
		return true;
	}

	@Override
	public int getPower() {
		return power;
	}

	@Override
	public int getStability() {
		return stability;
	}

	@Override
	public CoilType getType() {
		return type;
	}

	@Override
	public void readFromNbt(CompoundTag tag) {
		this.power = tag.getInt("Power");
		this.stability = tag.getInt("Stability");
		this.type = CoilType.values()[tag.getInt("Type")];
		this.stabilityTicks = tag.getInt("StabilityTicks");
	}

	@Override
	public void writeToNbt(CompoundTag tag) {
		tag.putInt("Power", power);
		tag.putInt("Stability", stability);
		tag.putInt("Type", type.ordinal());
		tag.putInt("StabilityTicks", stabilityTicks);
	}
}
