package repulica.qi.impl;

import dev.onyxstudios.cca.api.v3.component.AutoSyncedComponent;
import repulica.qi.api.CoilComponent;
import repulica.qi.api.CoilType;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;


public class PlainCoilComponent implements CoilComponent, AutoSyncedComponent {
	private int power = 75;
	private int stability = 100;
	private CoilType type = CoilType.LEGAL;

	public PlainCoilComponent(ItemStack stack) { }

	@Override
	public boolean isMachine() {
		return false;
	}

	@Override
	public int getPower() {
		return power;
	}

	@Override
	public int getStability() {
		return stability;
	}

	@Override
	public CoilType getType() {
		return type;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public void setStability(int stability) {
		this.stability = stability;
	}

	public void setType(CoilType type) {
		this.type = type;
	}

	@Override
	public void readFromNbt(CompoundTag tag) {
		this.power = tag.getInt("Power");
		this.stability = tag.getInt("Stability");
		this.type = CoilType.values()[tag.getInt("Type")];
	}

	@Override
	public void writeToNbt(CompoundTag tag) {
		tag.putInt("Power", power);
		tag.putInt("Stability", stability);
		tag.putInt("Type", type.ordinal());
	}
}
