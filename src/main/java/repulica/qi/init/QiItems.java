package repulica.qi.init;

import repulica.qi.Qi;
import repulica.qi.item.CoilItem;

import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class QiItems {

	public static final Item COIL = register("coil", new CoilItem(new Item.Settings()));

	public static final Item RECOVERY_ORDER = register("recovery_order", new Item(new Item.Settings()));

	public static void init() {
	}

	private static Item register(String name, Item item) {
		return Registry.register(Registry.ITEM, new Identifier(Qi.MODID, name), item);
	}
}
