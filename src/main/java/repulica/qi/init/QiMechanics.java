package repulica.qi.init;

import java.util.ArrayList;
import java.util.List;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistryV3;
import dev.onyxstudios.cca.api.v3.item.ItemComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.item.ItemComponentInitializer;
import repulica.qi.Qi;
import repulica.qi.api.CoilComponent;
import repulica.qi.effect.QiStatusEffect;
import repulica.qi.impl.PlainCoilComponent;
import repulica.qi.loot.RandomCoilLootFunction;
import repulica.qi.loot.RecoveryOrderLootCondition;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectType;
import net.minecraft.loot.condition.LootConditionType;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.function.LootFunctionType;
import net.minecraft.tag.ServerTagManagerHolder;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.fabric.api.tag.TagRegistry;

public class QiMechanics implements ItemComponentInitializer {
	public static final ComponentKey<CoilComponent> COIL =
			ComponentRegistryV3.INSTANCE.getOrCreate(new Identifier(Qi.MODID, "coil"), CoilComponent.class);

	public static final StatusEffect CHARGED = register("charged",
			new QiStatusEffect(StatusEffectType.BENEFICIAL, 0x00FFFF));

	public static final Tag.Identified<EntityType<?>> COIL_DROPPING = (Tag.Identified<EntityType<?>>)
			TagRegistry.entityType(new Identifier(Qi.MODID, "coil_dropping"));

	public static final LootConditionType RECOVERY_ORDER_CONDITION = register("recovery_order",
			new LootConditionType(new RecoveryOrderLootCondition.Serializer()));

	public static final LootFunctionType RANDOM_COIL = register("random_coil",
			new LootFunctionType(new RandomCoilLootFunction.Serializer()));

	private static final List<Identifier> tableIds = new ArrayList<>();

	@Override
	public void registerItemComponentFactories(ItemComponentFactoryRegistry registry) {
		registry.registerFor(new Identifier(Qi.MODID, "coil"), COIL, PlainCoilComponent::new);
	}

	public static void init() {
		LootTableLoadingCallback.EVENT.register(((resourceManager, lootManager, id, supplier, setter) -> {
			if (tableIds.contains(id)) {
				FabricLootPoolBuilder builder = FabricLootPoolBuilder.builder()
						.withCondition(RecoveryOrderLootCondition.INSTANCE)
						.withEntry(ItemEntry.builder(QiItems.COIL).build())
						.withFunction(RandomCoilLootFunction.INSTANCE);
				supplier.withPool(builder.build());
			}
		}));
	}

	public static void prepareTableIds() {
		tableIds.clear();
		for (EntityType<?> type : ServerTagManagerHolder.getTagManager().getEntityTypes()
				.getTagOrEmpty(COIL_DROPPING.getId()).values()) {
			tableIds.add(type.getLootTableId());
		}
	}

	private static StatusEffect register(String name, StatusEffect effect) {
		return Registry.register(Registry.STATUS_EFFECT, new Identifier(Qi.MODID, name), effect);
	}

	private static LootConditionType register(String name, LootConditionType type) {
		return Registry.register(Registry.LOOT_CONDITION_TYPE, new Identifier(Qi.MODID, name), type);
	}

	private static LootFunctionType register(String name, LootFunctionType type) {
		return Registry.register(Registry.LOOT_FUNCTION_TYPE, new Identifier(Qi.MODID, name), type);
	}
}
