package repulica.qi.item;

import java.util.List;

import org.jetbrains.annotations.Nullable;
import repulica.qi.api.CoilComponent;
import repulica.qi.init.QiMechanics;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

public class CoilItem extends Item {

	public CoilItem(Settings settings) {
		super(settings);
	}

	@Environment(EnvType.CLIENT)
	@Override
	public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
		super.appendTooltip(stack, world, tooltip, context);
		CoilComponent component = QiMechanics.COIL.get(stack);
		if (Screen.hasShiftDown()) {
			tooltip.add(new TranslatableText("tooltip.qi.params").formatted(Formatting.GRAY));
			tooltip.add(new TranslatableText("tooltip.qi.power", component.getPower()).formatted(Formatting.GRAY));
			tooltip.add(new TranslatableText("tooltip.qi.stability", component.getStability()).formatted(Formatting.GRAY));
			Text type = component.getType().getName();
			if (type != null) {
				tooltip.add(new TranslatableText("tooltip.qi.type", type).formatted(Formatting.GRAY));
			}
		} else {
			tooltip.add(new TranslatableText("tooltip.qi.more").formatted(Formatting.GRAY));
		}
	}
}
