package repulica.qi.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import repulica.qi.api.CoilComponent;
import repulica.qi.api.CoilType;
import repulica.qi.impl.PlainCoilComponent;
import repulica.qi.init.QiMechanics;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.loot.function.LootFunctionType;
import net.minecraft.util.JsonSerializer;

public class RandomCoilLootFunction implements LootFunction {
	public static final RandomCoilLootFunction INSTANCE = new RandomCoilLootFunction();

	private RandomCoilLootFunction() {}

	@Override
	public LootFunctionType getType() {
		return QiMechanics.RANDOM_COIL;
	}

	@Override
	public ItemStack apply(ItemStack stack, LootContext context) {
		if (QiMechanics.COIL.isProvidedBy(stack)) {
			CoilComponent component = QiMechanics.COIL.get(stack);
			if (component instanceof PlainCoilComponent) {
				PlainCoilComponent comp = (PlainCoilComponent) component;
				//only legal or illegal coils from recovery orders
				if (context.getRandom().nextInt(4) == 0) {
					comp.setPower(context.getRandom().nextInt(20) + 40);
					comp.setStability(100);
					comp.setType(CoilType.LEGAL);
				} else {
					comp.setPower(context.getRandom().nextInt(20) + 60);
					comp.setStability(context.getRandom().nextInt(15) + (85 - comp.getPower() / 5));
					comp.setType(CoilType.ILLEGAL);
				}
			}
		}
		return stack;
	}

	public static class Serializer implements JsonSerializer<RandomCoilLootFunction> {
		public void toJson(JsonObject jsonObject, RandomCoilLootFunction function,
						   JsonSerializationContext jsonSerializationContext) {
		}

		public RandomCoilLootFunction fromJson(JsonObject jsonObject,
											   JsonDeserializationContext jsonDeserializationContext) {
			return RandomCoilLootFunction.INSTANCE;
		}
	}
}
