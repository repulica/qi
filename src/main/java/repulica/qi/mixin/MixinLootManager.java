package repulica.qi.mixin;

import java.util.Map;

import com.google.gson.JsonObject;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import repulica.qi.Qi;
import repulica.qi.init.QiMechanics;

import net.minecraft.loot.LootManager;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;

@Mixin(LootManager.class)
public class MixinLootManager {
	@Inject(method = "apply", at = @At("HEAD"))
	private void prepIds(Map<Identifier, JsonObject> resources, ResourceManager manager, Profiler profiler, CallbackInfo info) {
		QiMechanics.prepareTableIds();
	}
}
